const getFinalScore = require("./getFinalScore");
const addValueToBoard = require("./addValueToBoard");
const parseBoard = require("./parseBoard.js");
const checkWinning = require("./checkWinning");

const func = ({ draw, boards }) => {
  let parsedBoards = boards.map((b) => parseBoard(b));
  let winningBoard = null;
  let lastNum = null;

  draw.some((n) => {
    parsedBoards = parsedBoards.map((b) => {
      const pB = addValueToBoard(n, b);
      if (checkWinning(pB)) {
        lastNum = n;
        winningBoard = pB;
      }
      return pB;
    });
    return winningBoard;
  });

  return getFinalScore(lastNum, winningBoard);
};

module.exports = func;

// const data = require("./data.json");
// console.log(func(data.input));
