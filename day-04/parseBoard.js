const parseBoard = (board) =>
  board.map((r) => r.map((value) => ({ value, marked: false })));

module.exports = parseBoard;
