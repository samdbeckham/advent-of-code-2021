const checkRows = board => board.some(r => r.every(({marked}) => marked));
const checkCols = board => {
  const flippedBoard = board.map((r, i) => r.map((n, j) => board[j][i]));
  return checkRows(flippedBoard);
};

const checkWinning = board => {
  return checkRows(board) || checkCols(board);
};

module.exports = checkWinning;