const assert = require("assert");

const getFinalScore = require("./getFinalScore");
const addValueToBoard = require("./addValueToBoard");
const parseBoard = require("./parseBoard.js");
const checkWinning = require("./checkWinning.js");
const part1 = require("./part1");
const part2 = require("./part2");

const data = require("./data.json");
const emptyBoard = [
  [
    { value: 14, marked: false },
    { value: 21, marked: false },
    { value: 17, marked: false },
    { value: 24, marked: false },
    { value: 4, marked: false },
  ],
  [
    { value: 10, marked: false },
    { value: 16, marked: false },
    { value: 15, marked: false },
    { value: 9, marked: false },
    { value: 19, marked: false },
  ],
  [
    { value: 18, marked: false },
    { value: 8, marked: false },
    { value: 23, marked: false },
    { value: 26, marked: false },
    { value: 20, marked: false },
  ],
  [
    { value: 22, marked: false },
    { value: 11, marked: false },
    { value: 13, marked: false },
    { value: 6, marked: false },
    { value: 5, marked: false },
  ],
  [
    { value: 2, marked: false },
    { value: 0, marked: false },
    { value: 12, marked: false },
    { value: 3, marked: false },
    { value: 7, marked: false },
  ],
];
const winningRowBoard = [
  [
    { value: 14, marked: true },
    { value: 21, marked: true },
    { value: 17, marked: true },
    { value: 24, marked: true },
    { value: 4, marked: true },
  ],
  [
    { value: 10, marked: false },
    { value: 16, marked: false },
    { value: 15, marked: false },
    { value: 9, marked: true },
    { value: 19, marked: false },
  ],
  [
    { value: 18, marked: false },
    { value: 8, marked: false },
    { value: 23, marked: true },
    { value: 26, marked: false },
    { value: 20, marked: false },
  ],
  [
    { value: 22, marked: false },
    { value: 11, marked: true },
    { value: 13, marked: false },
    { value: 6, marked: false },
    { value: 5, marked: true },
  ],
  [
    { value: 2, marked: true },
    { value: 0, marked: true },
    { value: 12, marked: false },
    { value: 3, marked: false },
    { value: 7, marked: true },
  ],
];

const winningColBoard = [
  [
    { value: 14, marked: true },
    { value: 21, marked: false },
    { value: 17, marked: false },
    { value: 24, marked: false },
    { value: 4, marked: false },
  ],
  [
    { value: 10, marked: true },
    { value: 16, marked: false },
    { value: 15, marked: false },
    { value: 9, marked: false },
    { value: 19, marked: false },
  ],
  [
    { value: 18, marked: true },
    { value: 8, marked: false },
    { value: 23, marked: false },
    { value: 26, marked: false },
    { value: 20, marked: false },
  ],
  [
    { value: 22, marked: true },
    { value: 11, marked: false },
    { value: 13, marked: false },
    { value: 6, marked: false },
    { value: 5, marked: false },
  ],
  [
    { value: 2, marked: true },
    { value: 0, marked: false },
    { value: 12, marked: false },
    { value: 3, marked: false },
    { value: 7, marked: false },
  ],
];

describe("Day 4:", () => {
  const { example } = data;

  describe("helpers", () => {
    describe("parseBoard", () => {
      it("should parse an array to an object", () => {
        const result = parseBoard(example.boards[2]);

        assert.deepEqual(result, emptyBoard);
      });
    });

    describe("addValueToBoard", () => {
      it("should mark the correct value on a board", () => {
        const value = 10;
        const result = addValueToBoard(value, winningRowBoard);

        assert.strictEqual(winningRowBoard[1][0].marked, false);
        assert.strictEqual(result[1][0].marked, true);
      });

      describe("checkWinning", () => {
        it("should return true for a winning row", () => {
          const result = checkWinning(winningRowBoard);

          assert.strictEqual(result, true);
        });

        it("should return true for a winning col", () => {
          const result = checkWinning(winningColBoard);

          assert.strictEqual(result, true);
        });

        it("should return false for a losing board", () => {
          const result = checkWinning(emptyBoard);

          assert.strictEqual(result, false);
        });
      });

      it("should do nothing if the number isn't in the board", () => {
        const value = 999;
        const result = addValueToBoard(value, winningRowBoard);

        assert.deepEqual(winningRowBoard, result);
      });
    });

    describe("getFinalScore", () => {
      it("should sum the winning board correctly", () => {
        const lastNum = 24;
        const result = getFinalScore(lastNum, winningRowBoard);

        assert.strictEqual(result, 4512);
      });
    });
  });

  describe("Part 1:", () => {
    it("should pass the given example", () => {
      const result = part1(example);

      assert.strictEqual(result, 4512);
    });
  });

  describe("Part 2:", () => {
    it("should pass the given example", () => {
      const result = part2(example);

      assert.strictEqual(result, 1924);
    });
  });
});
