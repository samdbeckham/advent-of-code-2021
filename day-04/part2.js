const getFinalScore = require("./getFinalScore");
const addValueToBoard = require("./addValueToBoard");
const parseBoard = require("./parseBoard.js");
const checkWinning = require("./checkWinning");

const func = ({ draw, boards }) => {
  let parsedBoards = boards.map((b) => parseBoard(b));
  let losingBoard = null;
  let lastNum = null;

  draw.some((n) => {
    const lb = parsedBoards[0];
    lastNum = n;
    parsedBoards = parsedBoards.map((b) => addValueToBoard(n, b));
    parsedBoards = parsedBoards.filter((b) => !checkWinning(b));
    if (parsedBoards.length === 0) {
      losingBoard = addValueToBoard(n, lb);
    }
    return losingBoard;
  });

  return getFinalScore(lastNum, losingBoard);
};

module.exports = func;

// const data = require('./data.json');
// console.log(func(data.input));
