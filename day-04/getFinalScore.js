const getUnmarked = (board) =>
  board.reduce((acc, r) => [...acc, ...r]).filter((n) => !n.marked);
const sumBoard = (board) => board.reduce((acc, n) => acc + n.value, 0);

const getFinalScore = (lastNum, board) =>
  lastNum * sumBoard(getUnmarked(board));

module.exports = getFinalScore;