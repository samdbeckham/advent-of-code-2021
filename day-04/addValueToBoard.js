const addValueToBoard = (_value, board) =>
  board.map((r) =>
    r.map(({ value, marked }) => ({
      value,
      marked: marked || value === _value,
    }))
  );

module.exports = addValueToBoard;
