const func = (depths) =>
  depths.reduce(
    ({ prev, tot }, depth) =>
      depth > prev ? { prev: depth, tot: tot + 1 } : { prev: depth, tot },
    { prev: Math.infinity, tot: 0 }
  ).tot;

module.exports = func;

// const data = require("./data.json");
// console.log(func(data.part1.input));
