const getIncreases = require("./part1");

const getRolling = (depths) =>
  depths.map((depth, index) => {
    const b = depths[index + 1] || 0;
    const c = depths[index + 2] || 0;
    return depth + b + c;
  });

const func = (depths) => getIncreases(getRolling(depths, 3));

module.exports = func;

// const data = require("./data.json");
// console.log(func(data.part1.input));
