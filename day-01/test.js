const assert = require("assert");

const data = require("./data.json");
const part1 = require("./part1");
const part2 = require("./part2");

describe("Day 1:", () => {
  describe("Part 1:", () => {
    const { example } = data.part1;

    it("should pass the given example", () => {
      const result = part1(example);
      assert.strictEqual(result, 7);
    });

    it("should return 0 with no increases", () => {
      const result = part1([3, 2, 1]);
      assert.strictEqual(result, 0);
    });

    it("should return 3 with an unbroken run", () => {
      const result = part1([1, 2, 3, 4]);
      assert.strictEqual(result, 3);
    });

    it("should return 3 with a broken run", () => {
      const result = part1([1, 2, 3, 1, 2]);
      assert.strictEqual(result, 3);
    });
  });

  describe("Part 2:", () => {
    // the example data is the same for both
    const { example } = data.part1;

    it("should pass the given example", () => {
      const result = part2(example);
      assert.strictEqual(result, 5);
    });
  });
});
