#!/bin/bash

NUM=$1
DIR="day-`printf %02d $NUM`"

cp -r ./day_template/ $DIR

for input_file in `find $DIR`
do
  if [ ! -d "${input_file}" ]; then
    echo "Rendering file: ${input_file}"
    sed -E "s/\[N\]/${NUM}/g" "${input_file}" > "${input_file}_tmp"
    mv "${input_file}_tmp" "${input_file}"
  fi
done