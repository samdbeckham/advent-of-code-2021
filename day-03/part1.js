const func = (data) => {
  const banana = data.reduce((acc, num) => {
    const digits = num.split("");
    digits.forEach((digit, i) => {
      acc[i] = acc[i] || 0;
      if (digit === "1") {
        acc[i]++;
      } else {
        acc[i]--;
      }
    });
    return acc;
  }, []);

  const gamma = parseInt(banana.map((d) => (d > 0 ? 1 : 0)).join(""), 2);
  const epsilon = parseInt(banana.map((d) => (d < 0 ? 1 : 0)).join(""), 2);

  return epsilon * gamma;
};

module.exports = func;

// const data = require("./data.json");
// console.log(func(data.part1.input));
