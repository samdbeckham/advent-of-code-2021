const assert = require("assert");

const data = require("./data.json");
const part1 = require("./part1");
const part2 = require("./part2");

describe("Day 3:", () => {
  describe("Part 1:", () => {
    it("should pass the given example", () => {
      const { example } = data.part1;
      const result = part1(example);

      assert.strictEqual(result, 198);
    });
  });
  describe("Part 2:", () => {
    it("should pass the given example", () => {
      const { example } = data.part1;
      const result = part2(example);

      assert.strictEqual(result, 230);
    });
  });
});
