function mode(arr) {
  return arr
    .sort(
      (a, b) =>
        arr.filter((v) => v === a).length - arr.filter((v) => v === b).length
    )
    .pop();
}

const func = (data) => {
  const oxygen = data[0].split("").reduce((acc, _, i) => {
    const commonDigit = mode(acc.map((d) => d[i]));
    return acc.filter((d) => d[i] === commonDigit);
  }, data);

  const co2 = data[0].split("").reduce((acc, _, i) => {
    if (acc.length === 1) {
      return acc;
    }
    const commonDigit = acc.length === 2 ? "1" : mode(acc.map((d) => d[i]));
    return acc.filter((d) => d[i] !== commonDigit);
  }, data);

  return parseInt(oxygen, 2) * parseInt(co2, 2);
};

module.exports = func;

// const data = require("./data.json");
// console.log(func(data.part1.input));
