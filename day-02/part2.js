const func = (steps) => {
  const directions = steps.reduce(
    ({ h, v, a }, step) => {
      const [dir, val] = step.split(" ");
      const pVal = parseInt(val, 10);
      switch (dir) {
        case "forward":
          return { v: v + a * pVal, h: h + pVal, a };
        case "down":
          return { h, v, a: a + pVal };
        case "up":
          return { h, v, a: a - pVal };
      }
    },
    { h: 0, v: 0, a: 0 }
  );

  return directions.h * directions.v;
};

module.exports = func;

// const data = require("./data.json");
// console.log(func(data.part1.input));
