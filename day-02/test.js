const assert = require("assert");

const data = require("./data.json");
const part1 = require("./part1");
const part2 = require("./part2");

describe("Day 2:", () => {
  describe("Part 1:", () => {
    it("should pass the given example", () => {
      const { example } = data.part1;
      const result = part1(example);

      assert.strictEqual(result, 150);
    });

    it("should start at 0", () => {
      const result = part1([]);

      assert.strictEqual(result, 0);
    });
  });

  describe("Part 2:", () => {
    it("should pass the given example", () => {
      const { example } = data.part1;
      const result = part2(example);

      assert.strictEqual(result, 900);
    });
  });
});
