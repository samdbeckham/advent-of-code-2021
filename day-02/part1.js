const func = (steps) => {
  const directions = steps.reduce(
    ({ h, v }, step) => {
      const [dir, val] = step.split(" ");
      const pVal = parseInt(val, 10);
      switch (dir) {
        case "forward":
          return { v, h: h + pVal };
        case "down":
          return { h, v: v + pVal };
        case "up":
          return { h, v: v - pVal };
      }
    },
    { h: 0, v: 0 }
  );

  return directions.h * directions.v;
};

module.exports = func;

// const data = require("./data.json");
// console.log(func(data.part1.input));
