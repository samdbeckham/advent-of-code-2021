const assert = require("assert");

const data = require("./data.json");
const part1 = require("./part1");

describe("Day [N]:", () => {
  const { example } = data;
  describe.only("Part 1:", () => {
    it("should pass the given example", () => {
      const result = part1(example);

      assert.strictEqual(result, undefined);
    });
  });
});
